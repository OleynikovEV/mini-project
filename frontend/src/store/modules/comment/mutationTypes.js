export const SET_COMMENTS = 'setComments';
export const ADD_COMMENT = 'addComment';
export const DELETE_COMMENT = 'deleteComment';
export const SET_COMMENT = 'editComment';